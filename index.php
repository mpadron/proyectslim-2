<?php
/**
 * Step 1: Require the Slim Framework
 *
 * If you are not using Composer, you need to require the
 * Slim Framework and register its PSR-0 autoloader.
 *
 * If you are using Composer, you can skip this step.
 */
require 'Slim/Slim.php';

\Slim\Slim::registerAutoloader();

/**
 * Step 2: Instantiate a Slim application
 *
 * This example instantiates a Slim application using
 * its default settings. However, you will usually configure
 * your Slim application now by passing an associative array
 * of setting names and values into the application constructor.
 */
$app = new \Slim\Slim(array(
    'debug'=>true,
    'templates.path'=>'Plantillas'
));

/**
 * Step 3: Define the Slim application routes
 *
 * Here we define several Slim application routes that respond
 * to appropriate HTTP request methods. In this example, the second
 * argument for `Slim::get`, `Slim::post`, `Slim::put`, `Slim::patch`, and `Slim::delete`
 * is an anonymous function.
 */

// GET route
$app->get(
    '/',
    function () use ($app) {

        $app->render('vista_index.php',array('name'=>'Miguel Eduardo Padron'));


    }
);

// POST route
$app->post(
    '/user',
    function () use ($app) {
        //echo 'This is a POST route '. $_POST['loginFom'];
        //
        //$request = $app->request;
        //echo($request);
        $obtenreData= $app->request;
        $loginbd="mp";


        if ($loginbd==$obtenreData->post('loginFom')) {
            $app->render('vista_index.php',array('name2'=>$obtenreData->post('loginFom')));
        } else {

            $app->render('vista_index.php',array('name2'=>'Error dce login'));
        }


    }
);


$app->get(
    '/user/:name/:ape',
    function ($name,$ape) use ($app){
        echo 'Bienvenido a mi Sistemas el Sr: '.$name." ".$ape;
    }
);

// PUT route
$app->put(
    '/put',
    function () {
        echo 'This is a PUT route';
    }
);

// PATCH route
$app->patch('/patch', function () {
    echo 'This is a PATCH route';
});

// DELETE route
$app->delete(
    '/delete',
    function () {
        echo 'This is a DELETE route';
    }
);

/**
 * Step 4: Run the Slim application
 *
 * This method should be called last. This executes the Slim application
 * and returns the HTTP response to the HTTP client.
 */
$app->run();
